import React from 'react';
import {
  BrowserRouter as Router,
  Routes,
  Route
} from "react-router-dom";
import List from './components/List';
import Home from './components/Home';
import News from './components/News';
import Navigation from './components/Navigation';


function App() {
  return (
    <Router>
      <Navigation />
      <Routes>
        <Route exact path='/' element={<Home />} />
        <Route path='/list' element={<List />} />
        <Route path='/news' element={<News />} />
      </Routes>
    </Router>
  );
}

export default App;

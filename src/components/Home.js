import React from "react";
import { useTranslation } from "react-i18next";
import i18next from "i18next";

function Home() {
    const { t } = useTranslation();
    return(
        <div>
            <span>{t("home")}</span>
            <div>
                <button onClick={() => {
                    i18next.changeLanguage('en');
                }}>EN</button>
                <button onClick={() => {
                    i18next.changeLanguage('vi');
                }}>vi</button>
            </div>
        </div>
    );
}

export default Home;